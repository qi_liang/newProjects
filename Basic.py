import keyword

'''
标识符
(1) 第一个字符必须是字母表中的字母或者下划线_
(2) 标识符的其他部分由字母，数字和下划线组成
(3) 标识符对大小写敏感
'''

print("python 保留字")
print(keyword.kwlist)

# 第一种注释

"""
第二种注释
"""

'''
第三注释
'''

'''
python最具备特色的就是使用缩进来表示代码块，不需要使用大括号{}
缩进的空格是可变的，但是同一个代码块的缩进必须是相同的空格
'''

if True:
    print("Answer")
    print("True")
else:
    print("Answer")
    print("False")

'''
Python 通常是一行写完一条语句，但如果语句很长，我们可以使用反斜杠(\)来实现多行语句
'''
item_one = 1
item_two = 10
total = item_one + \
        item_two
print(total)

'''
在[]{}或者()中的多行语句，不需要使用反斜杠(\)，例如
'''
total = ["item_one","itme_two",
         "item_three"
         ]
print(total)

#数字类型
'''
python中数字有四种类型：整数 布尔 浮点数 复数
int(整数)
bool(布尔)
float(浮点)
complex(复数)
'''

#字符串
'''
python中单引号和双引号使用完全相同
使用三引号可以指定一个多行字符串
转移符 '\''
'''

